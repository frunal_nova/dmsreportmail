package com.DMSMail.Response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SMS")
public class SMS {

	@XmlElement(name = "Code")
	String Code;

	@XmlElement(name = "Description")
	String Desciption;

	@XmlElement(name = "AcceptedTime")
	String AcceptedTime;

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getDesciption() {
		return Desciption;
	}

	public void setDesciption(String desciption) {
		Desciption = desciption;
	}

	public String getAcceptedTime() {
		return AcceptedTime;
	}

	public void setAcceptedTime(String acceptedTime) {
		AcceptedTime = acceptedTime;
	}

}
