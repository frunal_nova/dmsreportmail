package com.DMSMail.Response;

public class AdvisorOffer {
	
	private String advisorName;
	
	private String LICustomer;
	
	private String HICustomer;
	
	private String HILICustomer;
	
	private String advisorEmail;
	
	private String totalOffer;
	
	

	public String getTotalOffer() {
		return totalOffer;
	}

	public void setTotalOffer(String totalOffer) {
		this.totalOffer = totalOffer;
	}

	public String getAdvisorName() {
		return advisorName;
	}

	public void setAdvisorName(String advisorName) {
		this.advisorName = advisorName;
	}

	public String getLICustomer() {
		return LICustomer;
	}

	public void setLICustomer(String lICustomer) {
		LICustomer = lICustomer;
	}

	public String getHICustomer() {
		return HICustomer;
	}

	public void setHICustomer(String hICustomer) {
		HICustomer = hICustomer;
	}

	public String getHILICustomer() {
		return HILICustomer;
	}

	public void setHILICustomer(String hILICustomer) {
		HILICustomer = hILICustomer;
	}

	public String getAdvisorEmail() {
		return advisorEmail;
	}

	public void setAdvisorEmail(String advisorEmail) {
		this.advisorEmail = advisorEmail;
	}
	
	
	

}
