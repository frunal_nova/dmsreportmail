package com.DMSMail.Response;

public class AdvisorPointsDetails {

	private String imanageId;
	
	private String name;
	
	private String mobNum;
	
	private String totalPoints;
	
	private String primaryPoints;
	
	private String totalSecondarypoints;
	
	private String secondaryPoints;
	
	private String activationPoints;
	
	private String userLevel;
	private String totalNextLevel;

	private String totalNextLevelPoints;

	private String minimumNextLevel;
	private String minimumNextLevelPoints;
	
	private String primaryBusiness;
	
	private String secondaryBusiness;
	
	private String kamEmailId;
	
	private String cspocEmail;
	
	private String userEmail;

	public String getImanageId() {
		return imanageId;
	}

	public void setImanageId(String imanageId) {
		this.imanageId = imanageId;
	}

	public String getMobNum() {
		return mobNum;
	}

	public void setMobNum(String mobNum) {
		this.mobNum = mobNum;
	}

	public String getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(String totalPoints) {
		this.totalPoints = totalPoints;
	}

	public String getPrimaryPoints() {
		return primaryPoints;
	}

	public void setPrimaryPoints(String primaryPoints) {
		this.primaryPoints = primaryPoints;
	}

	public String getTotalSecondarypoints() {
		return totalSecondarypoints;
	}

	public void setTotalSecondarypoints(String totalSecondarypoints) {
		this.totalSecondarypoints = totalSecondarypoints;
	}

	public String getSecondaryPoints() {
		return secondaryPoints;
	}

	public void setSecondaryPoints(String secondaryPoints) {
		this.secondaryPoints = secondaryPoints;
	}

	public String getActivationPoints() {
		return activationPoints;
	}

	public void setActivationPoints(String activationPoints) {
		this.activationPoints = activationPoints;
	}

	public String getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}

	public String getTotalNextLevelPoints() {
		return totalNextLevelPoints;
	}

	public void setTotalNextLevelPoints(String totalNextLevelPoints) {
		this.totalNextLevelPoints = totalNextLevelPoints;
	}

	public String getMinimumNextLevelPoints() {
		return minimumNextLevelPoints;
	}

	public void setMinimumNextLevelPoints(String minimumNextLevelPoints) {
		this.minimumNextLevelPoints = minimumNextLevelPoints;
	}

	public String getPrimaryBusiness() {
		return primaryBusiness;
	}

	public void setPrimaryBusiness(String primaryBusiness) {
		this.primaryBusiness = primaryBusiness;
	}

	public String getSecondaryBusiness() {
		return secondaryBusiness;
	}

	public void setSecondaryBusiness(String secondaryBusiness) {
		this.secondaryBusiness = secondaryBusiness;
	}

	public String getKamEmailId() {
		return kamEmailId;
	}

	public void setKamEmailId(String kamEmailId) {
		this.kamEmailId = kamEmailId;
	}

	public String getCspocEmail() {
		return cspocEmail;
	}

	public void setCspocEmail(String cspocEmail) {
		this.cspocEmail = cspocEmail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTotalNextLevel() {
		return totalNextLevel;
	}

	public void setTotalNextLevel(String totalNextLevel) {
		this.totalNextLevel = totalNextLevel;
	}

	public String getMinimumNextLevel() {
		return minimumNextLevel;
	}

	public void setMinimumNextLevel(String minimumNextLevel) {
		this.minimumNextLevel = minimumNextLevel;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	
	
}
