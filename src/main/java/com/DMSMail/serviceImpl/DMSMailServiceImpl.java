package com.DMSMail.serviceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.DMSMail.Response.AdvisorOffer;
import com.DMSMail.Response.AdvisorPointsDetails;
import com.DMSMail.Response.CustomMail;
import com.DMSMail.Response.SMS;
import com.DMSMail.model.AdvisorDetails;
import com.DMSMail.service.DMSMailService;
import com.DMSMail.utils.AesUtils;
import com.DMSMail.utils.DMSConstants;
import com.DMSMail.utils.DateUtil;
import com.DMSMail.utils.EmailUtil;
import com.DMSMail.utils.HttpPostClient;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

@Service
public class DMSMailServiceImpl implements DMSMailService {

	private static final Logger logger = LoggerFactory.getLogger(DMSMailServiceImpl.class);

	@Autowired
	private EmailUtil emailUtil;

	@Autowired
	private Configuration freemarkerConfig;

	@Autowired
	private AesUtils aesUtils;

	@Value("${reportEmail.filePath}")
	private String basefilePath;
	
	@Value("${pointsEmail.filePath}")
	private String pointfilePath;
	
	@Value("${customEmail.filePath}")
	private String customfilePath;

	@Value("${sendOfferListPASA.filePath}")
	private String offerListPASA;

	@Value("${sendOfferListEMPANELL.filePath}")
	private String offerListEMPANELL;
	
	@Value("${offerList.filePath}")
	private String offerList;

	@Autowired
	private HttpPostClient httpPostClient;

	int i = 0;

	@Override
	public void sendReportMails() throws Exception {
		boolean emailID = false;

		try {
			FileInputStream fis = new FileInputStream(basefilePath);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet worksheet = workbook.getSheetAt(0);
			for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
				AdvisorDetails advisorDetail = new AdvisorDetails();
				XSSFRow row = worksheet.getRow(i);
				if (!row.getCell(0).getStringCellValue().isEmpty()) {
					advisorDetail.setUserName(row.getCell(0).getStringCellValue());
					if (row.getCell(1) != null) {
						advisorDetail.setMobileNum(row.getCell(1).getStringCellValue());
						logger.info("--------------------------------------- After mobile number");
					} else {
						advisorDetail.setMobileNum(null);
					}
					if (row.getCell(2) != null) {
						advisorDetail.setEmailId(row.getCell(2).getStringCellValue());
					} else {
						advisorDetail.setEmailId(null);
					}

					logger.info("--------------------------------------- After Email ID");
					advisorDetail.setImanageId(row.getCell(3).getStringCellValue());
					advisorDetail.setCategory(row.getCell(4).getStringCellValue());
					logger.info("--------------------------------------- EMAIL ID " + advisorDetail.getEmailId());
					if (advisorDetail.getEmailId() != null && advisorDetail.getEmailId().contains("@")) {
						advisorDetail.setEmailId(advisorDetail.getEmailId().replace(" ", ""));
						emailID = true;
						formulateMails(advisorDetail, emailID);

					} else {
						emailID = false;
						formulateMails(advisorDetail, emailID);
						logger.info("Invalid email ID given for --------->" + row.getCell(0).getStringCellValue());

					}
				}
			}
			workbook.close();
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw e;
		}
	}

	private void formulateMails(AdvisorDetails advisorDetails, boolean emailID) throws Exception {
		logger.info(DMSConstants.METHOD_STARTS);
		i++;
		freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/emailTemplates/");
		String staffManageId = aesUtils.encrypt(aesUtils.getAlphaNumericString() + advisorDetails.getImanageId());
		URL url = new URL(DMSConstants.GENERATE_NEW_PWD_URL + URLEncoder.encode(staffManageId, "UTF-8"));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("staffName", advisorDetails.getUserName());
		model.put("url", url);

		String mailContent = FreeMarkerTemplateUtils
				.processTemplateIntoString(freemarkerConfig.getTemplate("welcomeMailHon.ftl"), model);

		emailUtil.sendMail(advisorDetails.getEmailId(), "SELECT Advisor Portal Launch", mailContent, null);

		
		/*
		 * if (i == 20) {
		 * logger.info("---------------------------------------> Inside thread sleep");
		 * Thread.sleep(75000); i = 0; }
		 */
		 

		/*
		 * if (advisorDetails.getMobileNum() != null &&
		 * !advisorDetails.getMobileNum().isEmpty()) {
		 * sendSMS(advisorDetails.getUserName(), url, advisorDetails.getMobileNum()); }
		 * else { logger.
		 * info("---------------------------------------> mobile number unavailable for "
		 * + advisorDetails.getImanageId()); }
		 */
		/*
		 * if (emailID) {
		 * 
		 * emailUtil.sendMail(advisorDetails.getEmailId(),
		 * "SELECT Advisor Portal Launch", mailContent);
		 * 
		 * if (advisorDetails.getMobileNum() != null &&
		 * !advisorDetails.getMobileNum().isEmpty()) {
		 * sendSMS(advisorDetails.getUserName(), url, advisorDetails.getMobileNum()); }
		 * else { logger.
		 * info("---------------------------------------> mobile number unavailable for "
		 * + advisorDetails.getImanageId()); }
		 * 
		 * } else { if (advisorDetails.getMobileNum() != null &&
		 * !advisorDetails.getMobileNum().isEmpty()) {
		 * sendSMS(advisorDetails.getUserName(), url, advisorDetails.getMobileNum()); }
		 * else { logger.
		 * info("---------------------------------------> mobile number unavailable for "
		 * + advisorDetails.getImanageId()); }
		 * 
		 * }
		 */

		logger.info(DMSConstants.METHOD_ENDS);
	}

	private void sendSMS(String userName, URL url, String mobileNumber) throws Exception {
		logger.info(DMSConstants.METHOD_STARTS);
		// for production url pass "PROD", for UAT url pass "UAT"

		String reqUrl = httpPostClient.formulateRequestURL(userName, url, mobileNumber);
		String response = httpPostClient.httpGetForSendSMS(reqUrl);
		SMS smsServiceResponse = httpPostClient.unMarshalXMLResponse(response);
		logger.info("------------------- SMS RESPONSE --------------- " + smsServiceResponse.getDesciption());
		logger.info(DMSConstants.METHOD_ENDS);

	}

	@Override
	public void sendOfferList() {
		logger.info(DMSConstants.METHOD_STARTS);

		try {
			for (int i = 1; i <= 2; i++) {

				if (i == 1) {
					/*
					 * FileInputStream fis = new FileInputStream(offerListEMPANELL);
					 */

					File file = new File(offerListEMPANELL);
					byte[] bytes = Files.readAllBytes(file.toPath());
					InputStreamSource attachmentSource = new ByteArrayResource(bytes);
					emailUtil.sendMailWithAttachment("Aparna.Samant@adityabirlacapital.com",
							"Empanell Leads From SELECT Portal", DMSConstants.SEND_EMPANELL_CONTENT,
							"EMPANELL_DATA.xlsx", attachmentSource);
				} else {
					File file = new File(offerListPASA);
					byte[] bytes = Files.readAllBytes(file.toPath());
					InputStreamSource attachmentSource = new ByteArrayResource(bytes);
					emailUtil.sendMailWithAttachment("Aparna.Samant@adityabirlacapital.com",
							"PASA Leads From SELECT Portal", DMSConstants.SEND_PASA_CONTENT, "PASA_LEAD_DATA.xlsx",
							attachmentSource);
				}

			}

			logger.info(DMSConstants.METHOD_ENDS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	@Override
	public void sendpointsTable() throws Exception {
		boolean emailID = false;
		int l=0;
		try {
			FileInputStream fis = new FileInputStream(pointfilePath);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			for(int j = 0;j<3;j++) {
				XSSFSheet worksheet = workbook.getSheetAt(0);
				for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
					AdvisorPointsDetails pointsDtl = new AdvisorPointsDetails();
					XSSFRow row = worksheet.getRow(i);
					if (!row.getCell(0).getStringCellValue().isEmpty()) {
						pointsDtl.setImanageId(String.valueOf(row.getCell(0).getStringCellValue()));
						logger.info(String.valueOf(l++));
						if (row.getCell(1) != null) {
							pointsDtl.setName(String.valueOf(row.getCell(1).getStringCellValue()));
						} else {
							pointsDtl.setName("NA");
						}
						logger.info(String.valueOf(l++));
						
						if (row.getCell(2) != null) {
							pointsDtl.setMobNum(String.valueOf(row.getCell(2).getStringCellValue()));
						} else {
							pointsDtl.setMobNum("NA");
						}
						logger.info(String.valueOf(l++));
						
						if (row.getCell(3) != null) {
							pointsDtl.setUserEmail(row.getCell(3).getStringCellValue());
						} else {
							pointsDtl.setTotalPoints("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(4) != null) {
							pointsDtl.setTotalPoints(String.valueOf(row.getCell(4).getStringCellValue()));
						} else {
							pointsDtl.setTotalPoints("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(5) != null) {
							pointsDtl.setPrimaryPoints(String.valueOf(row.getCell(5).getStringCellValue()));
						} else {
							pointsDtl.setPrimaryPoints("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(6) != null) {
							pointsDtl.setTotalSecondarypoints(String.valueOf(row.getCell(6).getStringCellValue()));
						} else {
							pointsDtl.setTotalSecondarypoints("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(7) != null) {
							pointsDtl.setSecondaryPoints(String.valueOf(row.getCell(7).getStringCellValue()));
						} else {
							pointsDtl.setSecondaryPoints("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(8) != null) {
							pointsDtl.setActivationPoints(String.valueOf(row.getCell(8).getStringCellValue()));
						} else {
							pointsDtl.setActivationPoints("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(9) != null) {
							pointsDtl.setUserLevel(String.valueOf(row.getCell(9).getStringCellValue()));
						} else {
							pointsDtl.setUserLevel("NA");
						}
						logger.info(String.valueOf(l++));
						
						if (row.getCell(10) != null) {
							pointsDtl.setTotalNextLevel(String.valueOf(row.getCell(10).getStringCellValue()));
						} else {
							pointsDtl.setUserLevel("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(11) != null) {
							pointsDtl.setTotalNextLevelPoints(String.valueOf(row.getCell(11).getStringCellValue()));
						} else {
							pointsDtl.setTotalNextLevelPoints("NA");
						}
						logger.info(String.valueOf(l++));
						
						if (row.getCell(12) != null) {
							pointsDtl.setMinimumNextLevel(String.valueOf(row.getCell(12).getStringCellValue()));
						} else {
							pointsDtl.setMinimumNextLevelPoints("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(13) != null) {
							pointsDtl.setMinimumNextLevelPoints(String.valueOf(row.getCell(13).getStringCellValue()));
						} else {
							pointsDtl.setMinimumNextLevelPoints("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(14) != null) {
							pointsDtl.setPrimaryBusiness(String.valueOf(row.getCell(14).getStringCellValue()));
						} else {
							pointsDtl.setPrimaryBusiness("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(15) != null) {
							pointsDtl.setSecondaryBusiness(String.valueOf(row.getCell(15).getStringCellValue()));
						} else {
							pointsDtl.setSecondaryBusiness("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(16) != null) {
							pointsDtl.setKamEmailId(String.valueOf(row.getCell(16).getStringCellValue()));
						} else {
							pointsDtl.setKamEmailId("NA");
						}
						logger.info(String.valueOf(l++));

						if (row.getCell(17) != null) {
							pointsDtl.setCspocEmail(String.valueOf(row.getCell(17).getStringCellValue()));
						} else {
							pointsDtl.setCspocEmail("NA");
						}
						logger.info(String.valueOf(l++));

						logger.info("--------------------------------------- EMAIL ID " + pointsDtl.getUserEmail());
						if (pointsDtl.getUserEmail() != null && pointsDtl.getUserEmail().contains("@")) {
							pointsDtl.setUserEmail(pointsDtl.getUserEmail().replace(" ", ""));
							emailID = true;
							formulatePointMails(pointsDtl, emailID);
						}

					}
					
				}
			}
			workbook.close();
			
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw e;
		}

	}

	private void formulatePointMails(AdvisorPointsDetails pointsDtl, boolean emailID) throws Exception, MessagingException {
		logger.info(DMSConstants.METHOD_STARTS);
		i++;
		freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/emailTemplates/");
		String staffManageId = aesUtils.encrypt(aesUtils.getAlphaNumericString() +pointsDtl.getImanageId());
		URL url = new URL(DMSConstants.GENERATE_NEW_PWD_URL + URLEncoder.encode(staffManageId, "UTF-8"));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("varName", pointsDtl.getName());
		model.put("varDate", DateUtil.getFormattedCurrentDate());
		model.put("varPoints", pointsDtl.getTotalPoints());
		model.put("varPriPoints", pointsDtl.getPrimaryPoints());
		model.put("varTotSecPts", pointsDtl.getTotalSecondarypoints());
		model.put("varSecPts", pointsDtl.getSecondaryPoints());
		model.put("varActPts", pointsDtl.getActivationPoints());
		model.put("varCurLvl", pointsDtl.getUserLevel());
		model.put("NxtLvl", pointsDtl.getTotalNextLevel());
		model.put("lvlPts",  pointsDtl.getTotalNextLevelPoints());
		model.put("minNxtLvl", pointsDtl.getMinimumNextLevel());
		model.put("NxtLvlpts", pointsDtl.getMinimumNextLevelPoints());
		model.put("PRIBIZ", pointsDtl.getPrimaryBusiness());
		model.put("SECONDARYBIZLIST", pointsDtl.getSecondaryBusiness());
		model.put("url", url);
		// model.put("category", advisorDetails.getCategory());

		String mailContent = FreeMarkerTemplateUtils
				.processTemplateIntoString(freemarkerConfig.getTemplate("pointsTable.ftl"), model);
		
		try {
			String ccMail = null;
			if(pointsDtl.getKamEmailId()!=null&&!pointsDtl.getKamEmailId().isEmpty()&&!pointsDtl.getKamEmailId().equalsIgnoreCase("")) {
				if(pointsDtl.getKamEmailId().contains("@")) {
					pointsDtl.setKamEmailId(pointsDtl.getKamEmailId().replace(" ",""));
					pointsDtl.setKamEmailId(pointsDtl.getKamEmailId().replace("<",""));
					pointsDtl.setKamEmailId(pointsDtl.getKamEmailId().replace(">",""));
					pointsDtl.setKamEmailId(pointsDtl.getKamEmailId().replace("?",""));
					pointsDtl.setKamEmailId(pointsDtl.getKamEmailId().replace(":",""));
					ccMail= pointsDtl.getKamEmailId();
				}
				
			}
			emailUtil.sendMail(pointsDtl.getUserEmail(), "SELECT Dashboard Snapshot", mailContent,ccMail);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		
		/*
		 * if (i == 20) {
		 * logger.info("---------------------------------------> Inside thread sleep");
		 * Thread.sleep(75000); i = 0; }
		 */
		 

		/*
		 * if (advisorDetails.getMobileNum() != null &&
		 * !advisorDetails.getMobileNum().isEmpty()) {
		 * sendSMS(advisorDetails.getUserName(), url, advisorDetails.getMobileNum()); }
		 * else { logger.
		 * info("---------------------------------------> mobile number unavailable for "
		 * + advisorDetails.getImanageId()); }
		 */
		/*
		 * if (emailID) {
		 * 
		 * emailUtil.sendMail(advisorDetails.getEmailId(),
		 * "SELECT Advisor Portal Launch", mailContent);
		 * 
		 * if (advisorDetails.getMobileNum() != null &&
		 * !advisorDetails.getMobileNum().isEmpty()) {
		 * sendSMS(advisorDetails.getUserName(), url, advisorDetails.getMobileNum()); }
		 * else { logger.
		 * info("---------------------------------------> mobile number unavailable for "
		 * + advisorDetails.getImanageId()); }
		 * 
		 * } else { if (advisorDetails.getMobileNum() != null &&
		 * !advisorDetails.getMobileNum().isEmpty()) {
		 * sendSMS(advisorDetails.getUserName(), url, advisorDetails.getMobileNum()); }
		 * else { logger.
		 * info("---------------------------------------> mobile number unavailable for "
		 * + advisorDetails.getImanageId()); }
		 * 
		 * }
		 */

		logger.info(DMSConstants.METHOD_ENDS);
	}

	@Override
	public void customMail() throws Exception {
		boolean emailID = false;

		try {
			FileInputStream fis = new FileInputStream(customfilePath);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet worksheet = workbook.getSheetAt(0);
			for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
				CustomMail customMail = new CustomMail();
				XSSFRow row = worksheet.getRow(i);
				if (!row.getCell(0).getStringCellValue().isEmpty()) {
					customMail.setEmailId(row.getCell(0).getStringCellValue());
					if (row.getCell(1) != null) {
						customMail.setAdId(row.getCell(1).getStringCellValue());
						logger.info("--------------------------------------- After mobile number");
					} else {
						customMail.setAdId(null);
					}
					
					logger.info("--------------------------------------- EMAIL ID " + customMail.getEmailId());
					if (customMail.getEmailId() != null && customMail.getEmailId().contains("@")) {
						customMail.setEmailId(customMail.getEmailId().replace(" ", ""));
						emailID = true;
						formulateCustomMails(customMail, emailID);

					} else {
						emailID = false;
						formulateCustomMails(customMail, emailID);
						logger.info("Invalid email ID given for --------->" + row.getCell(0).getStringCellValue());

					}
				}
			}
			workbook.close();
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw e;
		}
	}

	private void formulateCustomMails(CustomMail customMail, boolean emailID) throws Exception, MalformedTemplateNameException, ParseException, IOException, TemplateException {
		logger.info(DMSConstants.METHOD_STARTS);
		i++;
		freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/emailTemplates/");
		Map<String, Object> model = new HashMap<String, Object>();
		
		// model.put("category", advisorDetails.getCategory());

		String mailContent = FreeMarkerTemplateUtils
				.processTemplateIntoString(freemarkerConfig.getTemplate("KamTemplate.ftl"), model);
		
		try {
			if(customMail.getEmailId()!=null&&!customMail.getEmailId().isEmpty()&&!customMail.getEmailId().equalsIgnoreCase("")) {
				if(customMail.getEmailId().contains("@")) {
					customMail.setEmailId(customMail.getEmailId().replace(" ",""));
					customMail.setEmailId(customMail.getEmailId().replace("<",""));
					customMail.setEmailId(customMail.getEmailId().replace(">",""));
					customMail.setEmailId(customMail.getEmailId().replace("?",""));
					customMail.setEmailId(customMail.getEmailId().replace(":",""));
					
				}
				
			}
			emailUtil.sendMail(customMail.getEmailId(), "KAM Dashboard", mailContent,null);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info(DMSConstants.METHOD_ENDS);
	}

	@Override
	public void sendlobOfferList() throws Exception {
		boolean emailID = false;

		try {
			FileInputStream fis = new FileInputStream(offerList);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet worksheet = workbook.getSheetAt(0);
			for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
				AdvisorOffer advisorOffer = new AdvisorOffer();
				XSSFRow row = worksheet.getRow(i);
				if (!row.getCell(0).getStringCellValue().isEmpty()) {
					advisorOffer.setAdvisorName(row.getCell(0).getStringCellValue());
					if (row.getCell(1) != null) {
						advisorOffer.setLICustomer(row.getCell(1).getStringCellValue());
						logger.info("--------------------------------------- After mobile number");
					} else {
						advisorOffer.setLICustomer(null);
					}
					if (row.getCell(2) != null) {
						advisorOffer.setHICustomer(row.getCell(2).getStringCellValue());
					} else {
						advisorOffer.setHICustomer(null);
					}

					if (row.getCell(3) != null) {
						advisorOffer.setHILICustomer(row.getCell(3).getStringCellValue());
					} else {
						advisorOffer.setHILICustomer(null);
					}
					
					if (row.getCell(4) != null) {
						advisorOffer.setTotalOffer(row.getCell(4).getStringCellValue());
					} else {
						advisorOffer.setTotalOffer(null);
					}
					
					advisorOffer.setAdvisorEmail(row.getCell(5).getStringCellValue());
					logger.info("--------------------------------------- EMAIL ID " + advisorOffer.getAdvisorEmail());
					if (advisorOffer.getAdvisorEmail() != null && advisorOffer.getAdvisorEmail().contains("@")) {
						advisorOffer.setAdvisorEmail(advisorOffer.getAdvisorEmail().replace(" ", ""));
						emailID = true;
						formulateAdvisorOfferMails(advisorOffer, emailID);

					} else {
						emailID = false;
						formulateAdvisorOfferMails(advisorOffer, emailID);
						logger.info("Invalid email ID given for --------->" + row.getCell(0).getStringCellValue());

					}
				}
			}
			workbook.close();
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw e;
		}
	}

	private void formulateAdvisorOfferMails(AdvisorOffer advisorOffer, boolean emailID) throws Exception, MessagingException {
		logger.info(DMSConstants.METHOD_STARTS);
		i++;
		freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/emailTemplates/");
		Map<String, Object> model = new HashMap<String, Object>();
		String mailContent = null;
		model.put("staffName", advisorOffer.getAdvisorName());
		
		if (advisorOffer.getHILICustomer() != null && !advisorOffer.getHILICustomer().equalsIgnoreCase("0")) {
			model.put("count", advisorOffer.getTotalOffer());
			mailContent = FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfig.getTemplate("bothLIHIffers.ftl"), model);
			logger.info("INSIDE LI AND HI");
		} else if (advisorOffer.getHICustomer() != null && !advisorOffer.getHICustomer().equalsIgnoreCase("0")) {
			model.put("count", advisorOffer.getHICustomer());
			mailContent = FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfig.getTemplate("hiOffers.ftl"), model);
		} else if (advisorOffer.getLICustomer() != null && advisorOffer.getHICustomer().equalsIgnoreCase("0")) {
			model.put("count", advisorOffer.getLICustomer());
			mailContent = FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfig.getTemplate("liOffers.ftl"), model);
		} else {
			logger.info("NO CASES MATCHED");
		}

		emailUtil.sendMail(advisorOffer.getAdvisorEmail(), "SELECT Advisor Portal Launch", mailContent, null);


		logger.info(DMSConstants.METHOD_ENDS);
	}
}
