package com.DMSMail.utils;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;

@Component
public class AesUtils {

	public static final String SALT = "01234567890123456789012345678901";
	public static final String INITIAL_VECTOR = "98765432109876543210987654321010";
	public static final String KEYFORENCRYPTION = "L6b?_<yE08*brV<UWF.rq`a6c>FY66UH_`Z~r=b#XbSV7;#1,xuuUsyrM>s6A<>";
	public static final String KETFORCHECKSUM = "UWF.rq`a5c>FY6r=b#XbSV7;,xuuUsyrM>s0A<>L6b?_<yE08*brV<9UH_`Z~";
	public static final int AES_KEY_LENGTH = 128;
	public static final int AES_KEY_GENERATION_ITERATION_COUNT = 100;

	public String encrypt(String plaintext)
			throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException {
		if (plaintext != null) {
			SecretKey key = generateKey(SALT, KEYFORENCRYPTION);
			byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, INITIAL_VECTOR, plaintext.getBytes("UTF-8"));
			return Base64.getEncoder().encodeToString(encrypted);
		}
		return null;
	}
		
	public String decrypt(String ciphertext) throws Exception {
		if (ciphertext != null) {
			SecretKey key = generateKey(SALT, KEYFORENCRYPTION);
			byte[] sd = Base64.getDecoder().decode(ciphertext);
			byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, INITIAL_VECTOR, sd);
			return new String(decrypted, "UTF-8");
		}
		return null;
	}

	public String encryptChecksum(String plaintext)
			throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException {

		if (plaintext != null) {
			SecretKey key = generateKey(SALT, KETFORCHECKSUM);
			byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, INITIAL_VECTOR, plaintext.getBytes("UTF-8"));
			return Base64.getEncoder().encodeToString(encrypted);
		}
		return null;
	}

	private static SecretKey generateKey(String salt, String passphrase) {
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), hexStringtoByteArray(salt),
					AES_KEY_GENERATION_ITERATION_COUNT, AES_KEY_LENGTH);
			SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
			return key;
		} catch (Exception ignored) {
		}
		return null;
	}

	private static byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] ivArray = hexStringtoByteArray(iv);
			cipher.init(encryptMode, key, new IvParameterSpec(ivArray));
			return cipher.doFinal(bytes);
		} catch (Exception ignored) {
		}
		return null;
	}

	private static byte[] hexStringtoByteArray(String str) {
		int len = str.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
		}
		return data;
	}
		
	public final String getAlphaNumericString() {
		int n = 10;
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (AlphaNumericString.length() * Math.random());
			sb.append(AlphaNumericString.charAt(index));
		}
		return sb.toString();
	}
}
