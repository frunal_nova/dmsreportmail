package com.DMSMail.utils;

import java.security.spec.KeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CheckSumUtil {

	private static final Logger log= LoggerFactory.getLogger(CheckSumUtil.class);
	public static String GetCheckSum(String strToEncrypt) throws Exception {
		try {
			log.info(DMSConstants.METHOD_STARTS);
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");
			String dateTimeStamp = sdf.format(new Date());
			String saltValue = GenerateSaltValue(strToEncrypt, "FB1DF878-0B4B-481B-A90A-B575FC1ACF12", dateTimeStamp);
			String password = "Pass@w0rdLA";
			String IV = "e675f725e675f725";

			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(password.toCharArray(), saltValue.getBytes(), 1000, 128);
			SecretKey tmp = factory.generateSecret(spec);

			IvParameterSpec iv = new IvParameterSpec(IV.getBytes());
			SecretKeySpec skeySpec = new SecretKeySpec(tmp.getEncoded(), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(strToEncrypt.getBytes());
			String encryptedChecksum = Base64.getEncoder().encodeToString(encrypted);
			log.info(DMSConstants.METHOD_ENDS);
			return encryptedChecksum;

		} catch (RuntimeException | IllegalBlockSizeException | BadPaddingException ex) {
			throw ex;
		}
	}

	public static String GetCheckSum(String strToEncrypt, String timeStamp) throws Exception {
		try {
			log.info(DMSConstants.METHOD_STARTS);
			timeStamp = convertTimeStamp(timeStamp);
			String saltValue = GenerateSaltValue(strToEncrypt, "FB1DF878-0B4B-481B-A90A-B575FC1ACF12", timeStamp);
			String password = "Pass@w0rdLA";
			String IV = "e675f725e675f725";

			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(password.toCharArray(), saltValue.getBytes(), 1000, 128);
			SecretKey tmp = factory.generateSecret(spec);

			IvParameterSpec iv = new IvParameterSpec(IV.getBytes());
			SecretKeySpec skeySpec = new SecretKeySpec(tmp.getEncoded(), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(strToEncrypt.getBytes());
			String encryptedChecksum = Base64.getEncoder().encodeToString(encrypted);
			log.info(DMSConstants.METHOD_ENDS);
			return encryptedChecksum;

		} catch (RuntimeException | IllegalBlockSizeException | BadPaddingException ex) {
			throw ex;
		}
	}
	
	private static String convertTimeStamp(String timeStamp) {
        String time = "";
        SimpleDateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        SimpleDateFormat DesiredFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.ENGLISH);
        Date date1 = null;
        try {
            date1 = sourceFormat.parse(timeStamp);
            time = DesiredFormat.format(date1);
            time = time.replace("/","").replace(" ","").replace(":","");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

	public static String GenerateSaltValue(String strToEncrypt, String SaltValue, String timestamp) {
		List<Integer> position = SplitByLength(timestamp, 2);
		StringBuilder key = new StringBuilder();
		int len = strToEncrypt.length();
		for (int x : position) {
			if ((len > x)) {
				if (((x % 2) == 0)) {
					key.append(strToEncrypt.charAt(x));
				} else {
					key.append(strToEncrypt.charAt(len - 1 - x));
				}

			} else {
				int newLen = strToEncrypt.length();
				String newStr = strToEncrypt;
				while (((newLen - 1) < x)) {
					newStr = (newStr + newStr);
					newLen = newStr.length();
				}

				key.append(newStr.charAt(x));
			}

		}

		return ((key.toString().substring(0, (position.size() / 2))) + SaltValue
				+ (key.toString().substring((position.size() / 2))));
	}

	public static List<Integer> SplitByLength(String x, int maxLength) {
		List<Integer> a = new ArrayList<>();
		for (int i = 0; i < x.length(); i += 2) {
			if ((i + 2) < x.length()) {
				a.add(Integer.parseInt(x.substring(i, maxLength)) % 10);
			} else {
				a.add(Integer.parseInt(x.substring(i)) % 10);
			}
			maxLength = maxLength + 2;
		};
		return a;
	}

	public static String getCurrentTimeStamp() {
		try {
			log.info(DMSConstants.METHOD_STARTS);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
			String DateTimeStamp = simpleDateFormat.format(new Date());
			log.info(DMSConstants.METHOD_ENDS);
			return DateTimeStamp;
		} catch (Exception e) {
			throw e;
		}
	}
}
