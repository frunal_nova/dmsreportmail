package com.DMSMail.utils;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.DMSMail.Response.SMS;


/**
 * Created by Stephen Jebaraj on 10/23/2017.
 */

@Component
public class HttpPostClient {

	private static final Logger logger = LoggerFactory.getLogger(HttpPostClient.class);

	/*
	 * @Value("${sms.email.env}") private String smsEmailEnv;
	 * 
	 * @Value("${uat.rcpt.mobileNo}") private String uatRcptMobileNo;
	 */
	
	public static String callHttpPost(String url, ConcurrentMap<String, String> headersMap,
			ConcurrentMap<String, String> params, String jsonRequest) {
		String strResponse = "";
		HttpClient client = null;
		String protocol = null;
		try {
			if (url != null && !"".equals(url)) {
				protocol = url.split(":")[0];
			} else {
				throw new Exception("URL is null");
			}
			if ("HTTPS".equalsIgnoreCase(protocol)) {
				TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					public void checkClientTrusted(X509Certificate[] certs, String authType) {
					}

					public void checkServerTrusted(X509Certificate[] certs, String authType) {
					}
				} };

				HttpClientBuilder builder = HttpClientBuilder.create();
				SSLContext ctx = SSLContext.getInstance("TLS");
				ctx.init(null, trustAllCerts, null);
				SSLConnectionSocketFactory scsf = new SSLConnectionSocketFactory(ctx,SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
				builder.setSSLSocketFactory(scsf);
				Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
						.register("https", scsf).build();

				HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);

				builder.setConnectionManager(ccm);

				client = builder.build();

			} else {

				client = HttpClientBuilder.create().build();
			}

			HttpPost post = new HttpPost(url);

			// logic for param
			if (params != null && !"".equals(params)) {
				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				Set<String> set = params.keySet();
				Iterator<String> iterator = set.iterator();
				while (iterator.hasNext()) {
					String key = iterator.next();
					urlParameters.add(new BasicNameValuePair(key, params.get(key)));
				}
				post.setEntity(new UrlEncodedFormEntity(urlParameters));
			}
			// logic for headers
			if (headersMap != null && !"".equals(headersMap)) {
				Set<String> headerKeySet = headersMap.keySet();
				Iterator<String> headerKeyIterator = headerKeySet.iterator();
				while (headerKeyIterator.hasNext()) {
					String headerKey = headerKeyIterator.next();
					post.setHeader(headerKey, headersMap.get(headerKey));
				}
			}

			// logic for binary data in json format
			if (jsonRequest != null && !"".equals(jsonRequest)) {
				post.setEntity(new StringEntity(jsonRequest, ContentType.create("application/json")));
			}
			ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
				@Override
				public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
					int status = response.getStatusLine().getStatusCode();
					if (status >= 200 && status < 300) {
						HttpEntity entity = response.getEntity();
						return entity != null ? EntityUtils.toString(entity) : null;
					} else {
						throw new ClientProtocolException("Unexpected response status: " + status);
					}
				}
			};
			strResponse = client.execute(post, responseHandler);
		} catch (ClientProtocolException cpe) {
			System.out.println(" ClientProtocolException  " + cpe);
		} catch (Exception e) {
			System.out.println(" IOException  " + e);
		}
		return strResponse;

	}

	public static String httpPost(String url, String requestJson) {
		logger.info(DMSConstants.METHOD_STARTS);
		String rawResponse = null;
		try {
			String userName = "abfsgext/SSEXESB0040";
			String passWord = "2F5509E5#3A03!4BD9$B167#8C6C084EE5DWHU";
			String encoding = Base64.getEncoder().encodeToString((userName+ ":" +passWord).getBytes());
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			StringEntity entity = new StringEntity(requestJson);
			String checkSumValue = CheckSumUtil.GetCheckSum("ARN-22717");
			logger.info("-----------------------------------------------------------", checkSumValue);
			httpPost.setEntity(entity);
			httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			httpPost.setHeader("DateTimeStamp", CheckSumUtil.getCurrentTimeStamp());
			logger.info("-----------------------------------------------------------",CheckSumUtil.getCurrentTimeStamp());
			httpPost.setHeader("TokenID", "BAAE77CF-53C0-481C-BF74-4C5C723A5117");
			httpPost.setHeader(HttpHeaders.AUTHORIZATION,"Basic" + encoding);
			httpPost.setHeader("Checksum",checkSumValue);
			
			CloseableHttpResponse response = client.execute(httpPost);
			rawResponse = EntityUtils.toString(response.getEntity(), "UTF-8");
			
		} catch (Exception e) {
			logger.error(DMSConstants.INTERNAL_SERVER_ERROR);
		}
		logger.info(DMSConstants.METHOD_ENDS);
		return rawResponse;
	}
	
	public final String httpGetForSendSMS(String requestUrl) throws Exception {
		logger.info(DMSConstants.METHOD_STARTS);
		HttpClient httpclient = new DefaultHttpClient();
		try {
			
			requestUrl = requestUrl.replaceAll(" ", "%20");
			// Define a HttpGet request; You can choose between HttpPost, HttpDelete or
			// HttpPut also.
			HttpGet getRequest = new HttpGet(requestUrl);

			// Set the API media type in http accept header
			getRequest.addHeader("accept", "text/xml");
			HttpResponse response = httpclient.execute(getRequest);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != 200) {
				throw new Exception("Failed with HTTP error code : " + statusCode);
			}
			HttpEntity httpEntity = response.getEntity();
			String apiOutput = EntityUtils.toString(httpEntity);
			logger.info(DMSConstants.METHOD_ENDS);
			return apiOutput;
		} finally {
			// Important: Close the connect
			logger.info("-------------------------------------- INSIDE FINALLY BLOCK");
			httpclient.getConnectionManager().shutdown();
			
		}
	}
	
	
	public final String formulateRequestURL(String userName, URL url, String mobileNum)
			throws Exception {
		String reqUrl = null;
		String rcptMobileNo = null;
		Date date = new  Date();
		if(date.getDay()==1) {
			String smsText ="Dear #NAME#\r\n"
					+ "\r\n"
					+ "New special offer for your customers, mean new business opportunities for you. Use the SAP portal to view new special offer for your customers.\r\n"
					+ "Link: www.abcselect.adityabirlacapital.com\r\n"
					+ "Password: #PWDURL#\r\n"
					+ "\r\n"
					+ "Regards,\r\n"
					+ "Aditya Birla Capital Select Team";
			
			smsText = smsText.replace("#NAME#", userName);
			smsText = smsText.replace("#PWDURL#", url.toString());
			logger.info("----------------------------------------------smsText "+ smsText);
			reqUrl = DMSConstants.SEND_SMS_PROD_URL + "UserID=" + DMSConstants.SEND_SMS_PROD_USERID + "&Password="
					+ DMSConstants.SEND_SMS_PROD_PASSWORD + "&MobileNo=" +mobileNum+ "&SMSText="+URLEncoder.encode(smsText,"UTF8");
			logger.info("----------------------------------------------Request URL "+ reqUrl);
		}else if(date.getDay()==3) {
			String smsText = "Dear "+userName+"\r\n"
					+ "\r\n"
					+ "Over 600 users are already using the SAP Portal to see new offers for their customers. Login now and explore new business opportunities.\r\n"
					+ "Link: www.abcselect.adityabirlacapital.com\r\n"
					+ "Password: "+url+"\r\n"
					+ "\r\n"
					+ "Regards,\r\n"
					+ "Team Select \r\n"
					+ "";
			
			
			reqUrl = DMSConstants.SEND_SMS_PROD_URL + "UserID=" + DMSConstants.SEND_SMS_PROD_USERID + "&Password="
					+ DMSConstants.SEND_SMS_PROD_PASSWORD + "&MobileNo=" +mobileNum+ "&SMSText="+URLEncoder.encode(smsText,"UTF8");
			logger.info("----------------------------------------------Request URL "+ reqUrl);
		}else if(date.getDay()==5) {
			String smsText = "Dear "+userName+" \r\n"
					+ "\r\n"
					+ "New offers for your existing customers and a chance to boost your business. Login to the SAP portal and help yourself.\r\n"
					+ "Link: www.abcselect.adityabirlacapital.com \r\n"
					+ "Password: "+url+"\r\n"
					+ "\r\n"
					+ "Regards,\r\n"
					+ "Team Select\r\n"
					+ "";
			
			reqUrl = DMSConstants.SEND_SMS_PROD_URL + "UserID=" + DMSConstants.SEND_SMS_PROD_USERID + "&Password="
					+ DMSConstants.SEND_SMS_PROD_PASSWORD + "&MobileNo=" +mobileNum+ "&SMSText="+URLEncoder.encode(smsText,"UTF8");
			logger.info("----------------------------------------------Request URL "+ reqUrl);
		}else {
			logger.info("-------------------------------- NO MATCHED DATE");
		}
		

		return reqUrl;
	}
	
	public final SMS unMarshalXMLResponse(String response) throws Exception {
		JAXBContext jaxbContext;
		SMS smsServiceResponse = new SMS();
		jaxbContext = JAXBContext.newInstance(SMS.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		smsServiceResponse = (SMS) jaxbUnmarshaller.unmarshal(new StringReader(response));
		return smsServiceResponse;
	}

}
