package com.DMSMail.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Component
public class DateUtil {
    private static final Logger log= LoggerFactory.getLogger(DateUtil.class);

    public final static String  getFormattedDate(String format){
        if(format.contains("T")){
            Date sysDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return  sdf.format(sysDate);
        }else{
            SimpleDateFormat f = new SimpleDateFormat(format);
            return f.format(new Date());

        }

    }

    public final static Date getUtilDate(String strdate,String format){
        Date date=null;
       try {
          date= new SimpleDateFormat(format).parse(strdate);
       }catch (ParseException pe){
           log.error("ParserException :: ",pe);
       }
        return date;
    }
    
    public final static String subtractNdays() {

		Calendar now = Calendar.getInstance();
	
	    now.add(Calendar.DATE, -30);
	    String subDate = "0"+now.get(Calendar.DATE) + "-" + "0" +(now.get(Calendar.MONTH) + 1) + "-" +now.get(Calendar.YEAR);
	          
		return subDate;
    }
    
    
    
	public final static String getFormattedCurrentDate() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String currentTime = simpleDateFormat.format(new Date());
		return currentTime;
	}
	
	public final String getFormattedCurrentYear() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		String currentTime = simpleDateFormat.format(new Date());
		return currentTime;
	}
    
	
	public final String formatInputDate(String date) {
        String formattedDate ="";
        try {
            DateFormat frmUser = new SimpleDateFormat("yyyy-mm-dd");
            Date fromUser;
            fromUser = frmUser.parse(date);

            DateFormat df = new SimpleDateFormat("dd-mm-yyyy");
            formattedDate = df.format(fromUser);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedDate;
    }
	

}
