package com.DMSMail.utils;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailUtil {

	@Autowired
	private JavaMailSender sender;

	private static final Logger logger = LoggerFactory.getLogger(EmailUtil.class);

	public void sendMail(String sendTo, String subject, String mailContent, String ccMailId)
			throws MessagingException, UnsupportedEncodingException {

		//logger.info(DMSConstants.METHOD_STARTS);

		MimeMessage mimeMessage = sender.createMimeMessage();

		MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
		mimeMessageHelper.setSubject(subject);
		mimeMessageHelper
				.setFrom(new InternetAddress("abc.selectcare@adityabirlacapital.com", "ABC Select"));
		mimeMessageHelper.addCc(new InternetAddress("abc.selectcare@adityabirlacapital.com", "ABC Select"));
		if(ccMailId!=null) {
			mimeMessageHelper.addCc(ccMailId);
		}
		
		mimeMessageHelper.setTo(sendTo);
		mimeMessageHelper.setText(mailContent,true);
		

		logger.info("Mail sent to : ------------------> " +sendTo);
		sender.send(mimeMessageHelper.getMimeMessage());

	}
	
	public void sendMailWithAttachment(String sendTo, String subject, String mailContent, String fileName, InputStreamSource inputStreamSource)
			throws MessagingException, UnsupportedEncodingException {

		//logger.info(DMSConstants.METHOD_STARTS);

		MimeMessage mimeMessage = sender.createMimeMessage();

		MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
		mimeMessageHelper.setSubject(subject);
		mimeMessageHelper
				.setFrom(new InternetAddress("abc.select@adityabirlacapital.com", "ABC Select"));
		mimeMessageHelper.setTo(sendTo);
		mimeMessageHelper.setText(mailContent);
		mimeMessageHelper.addAttachment(fileName, inputStreamSource);

		logger.info("Mail sent to : ------------------> " +sendTo);
		sender.send(mimeMessageHelper.getMimeMessage());

	}
	
}
