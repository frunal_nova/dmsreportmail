package com.DMSMail.utils;



public class DMSConstants {
	//JWT Constants
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5*60*60;
    public static final String SIGNING_KEY = "dmsLoginValidationKey";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

	//error codes
	public static final String  E001 = "E001";
	public static final String  E002 = "E002";
	public static final String  E003 = "E003";
	public static final String  E004 = "E004";
	public static final String  E005 = "E005";
	
	//success codes
	public static final String  S001 = "S001";
	public static final String  S002 = "S002";

	
	public static final String  SUCCESS = "Success";
	public static final String  FAILURE = "Failure";
	public static final String  CONN_FAIL = "A system error occurred. Please contact administrator.";	
	public static final String  DATA_FOUND = "Data Found";
	public static final String  DATA_NOT_FOUND = "Data Not Found!";	
	public static final String  METHOD_STARTS = "METHOD STARTS";
	public static final String  METHOD_ENDS = "METHOD ENDS";
	public static final String 	INTERNAL_SERVER_ERROR="INTERNAL_SERVER_ERROR";
	
	//Send SMS Constants
	public static final String SEND_SMS_UAT_URL="http://10.158.1.60/WS_SMS_UAT/Service.asmx/SendSMS?";
	public static final String SEND_SMS_PROD_URL="http://10.158.1.60/WS_SMS/Service.asmx/SendSMS?";
	public static final String SEND_SMS_UAT_USERID = "499";
	public static final String SEND_SMS_UAT_PASSWORD = "ss@499";	
	public static final String SEND_SMS_PROD_USERID = "512";
	public static final String SEND_SMS_PROD_PASSWORD = "birla_512";
	
	// SMS & EMAIL Contents
	public static final String SEND_SMS_OTP_CONTENT = " is the OTP to authenticate your credentials for SELECT portal. Do not disclose the OTP to anyone.";
	public static final String SEND_FORGOT_PWD_SMS_OTP_CONTENT = " is the OTP to update your contact details at SELECT portal. Do not disclose the OTP to anyone.";
	public static final String SEND_MYPROFILE_OTP_CONTENT = " is your OTP to change your contact details for SELECT portal. Do not disclose the OTP to anyone. ";
	
	public static final String SEND_EMAIL_OTP_CONTENT = " is the OTP to authenticate your credentials for SELECT portal. OTP is valid for 15 minutes from the time it is generated. Do not disclose the OTP to anyone\r\n" + 
			"\r\n" + 
			"Thanks & Regards,\r\n" + 
			"SELECT Team";

	public static final String SEND_EMAIL_MYPROFILE_OTP_CONTENT = "is the OTP to authenticate the contact update for SELECT portal. OTP is valid for 15 minutes from the time it is generated. Do not disclose the OTP to anyone\r\n" + 
			"\r\n" + 
			"Thanks & Regards,\r\n" + 
			"Select Team";
	//Email URLS
	public static final String GENERATE_NEW_PWD_URL =  "https://www.abcselect.adityabirlacapital.com/create-pwd?";


	public static final String SEND_EMPANELL_CONTENT = "Dear Team,\r \n" + 
			"Please find empanelled leads data from SELECT Portal as attachment.\r\n" +
			"\r\n" +
			"Thanks & Regards,\r\n" + 
			"SELECT Team";
	
	public static final String SEND_PASA_CONTENT = "Dear Team,\r \n" + 
			"Please find PASA leads data from SELECT Portal as attachment.\r\n" +
			"\r\n" +
			"Thanks & Regards,\r\n" + 
			"SELECT Team";

	
}
