package com.DMSMail.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.DMSMail.exception.CustomException;
import com.DMSMail.exception.MailExceptionHandle;
import com.DMSMail.model.DMSMailStatus;
import com.DMSMail.service.DMSMailService;
import com.DMSMail.utils.DMSConstants;

@RestController
@RequestMapping("/api/imanage/v1")
public class MailResource {

	private static final Logger logger = LoggerFactory.getLogger(MailResource.class);
	
	@Autowired
	private DMSMailService dmsMailService;
	
	@Autowired
	private MailExceptionHandle mailExceptionHandle;

	@RequestMapping(value = "/sendWelcomeMail", method = RequestMethod.GET)
	public ResponseEntity<DMSMailStatus> sendMail() throws Exception{
		logger.info(DMSConstants.METHOD_STARTS);
		DMSMailStatus dmsMailStatus = new  DMSMailStatus();
		try {
			dmsMailService.sendReportMails();
		} catch (Exception e) {
			dmsMailStatus = mailExceptionHandle.dmsExceptionHandle(e.getMessage(), "E001");
			return ResponseEntity.ok().body(dmsMailStatus);
		}
		logger.info(DMSConstants.METHOD_ENDS);
		dmsMailStatus.setStatusCode("S001");
		dmsMailStatus.setStatusInfo("Email sent successfully");
		return ResponseEntity.ok().body(dmsMailStatus);
	}
	
	
	@RequestMapping(value = "/pointsTable", method = RequestMethod.GET)
	public ResponseEntity<DMSMailStatus> sendpointsTable() throws Exception{
		logger.info(DMSConstants.METHOD_STARTS);
		DMSMailStatus dmsMailStatus = new  DMSMailStatus();
		try {
			dmsMailService.sendpointsTable();
		} catch (Exception e) {
			dmsMailStatus = mailExceptionHandle.dmsExceptionHandle(e.getMessage(), "E001");
			return ResponseEntity.ok().body(dmsMailStatus);
		}
		logger.info(DMSConstants.METHOD_ENDS);
		dmsMailStatus.setStatusCode("S001");
		dmsMailStatus.setStatusInfo("Email sent successfully");
		return ResponseEntity.ok().body(dmsMailStatus);
	}
	
	@RequestMapping(value = "/customMail", method = RequestMethod.GET)
	public ResponseEntity<DMSMailStatus> customMail() throws Exception{
		logger.info(DMSConstants.METHOD_STARTS);
		DMSMailStatus dmsMailStatus = new  DMSMailStatus();
		try {
			dmsMailService.customMail();
		} catch (Exception e) {
			dmsMailStatus = mailExceptionHandle.dmsExceptionHandle(e.getMessage(), "E001");
			return ResponseEntity.ok().body(dmsMailStatus);
		}
		logger.info(DMSConstants.METHOD_ENDS);
		dmsMailStatus.setStatusCode("S001");
		dmsMailStatus.setStatusInfo("Email sent successfully");
		return ResponseEntity.ok().body(dmsMailStatus);
	}
	
	@RequestMapping(value = "/sendOfferList", method = RequestMethod.GET)
	public ResponseEntity<DMSMailStatus> sendOfferList() throws Exception{
		logger.info(DMSConstants.METHOD_STARTS);
		DMSMailStatus dmsMailStatus = new  DMSMailStatus();
		try {
			dmsMailService.sendOfferList();
		} catch (Exception e) {
			dmsMailStatus = mailExceptionHandle.dmsExceptionHandle(e.getMessage(), "E001");
			return ResponseEntity.ok().body(dmsMailStatus);
		}
		logger.info(DMSConstants.METHOD_ENDS);
		dmsMailStatus.setStatusCode("S001");
		dmsMailStatus.setStatusInfo("Email sent successfully");
		return ResponseEntity.ok().body(dmsMailStatus);
	}
	
	@RequestMapping(value = "/lobOfferList", method = RequestMethod.GET)
	public ResponseEntity<DMSMailStatus> sendlobOfferList() throws Exception{
		logger.info(DMSConstants.METHOD_STARTS);
		DMSMailStatus dmsMailStatus = new  DMSMailStatus();
		try {
			dmsMailService.sendlobOfferList();
		} catch (Exception e) {
			dmsMailStatus = mailExceptionHandle.dmsExceptionHandle(e.getMessage(), "E001");
			return ResponseEntity.ok().body(dmsMailStatus);
		}
		logger.info(DMSConstants.METHOD_ENDS);
		dmsMailStatus.setStatusCode("S001");
		dmsMailStatus.setStatusInfo("Email sent successfully");
		return ResponseEntity.ok().body(dmsMailStatus);
	}

}
