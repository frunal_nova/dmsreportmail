package com.DMSMail.exception;

public class CustomException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8181834625568218764L;

	public CustomException() {
        super("Exception created in custom way");
    }
	
}
