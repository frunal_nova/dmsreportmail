package com.DMSMail.exception;

import org.springframework.stereotype.Component;

import com.DMSMail.model.DMSMailStatus;

@Component
public class MailExceptionHandle {

	public DMSMailStatus dmsExceptionHandle(String message, String code) {
		DMSMailStatus statusInfo = new DMSMailStatus();
		statusInfo.setStatusCode(code);
		statusInfo.setStatusInfo(message);
		return statusInfo;
	}
}
