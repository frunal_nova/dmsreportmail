package com.DMSMail.exception;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlers extends ResponseEntityExceptionHandler {

	 @ExceptionHandler(CustomException.class)
	    public ResponseEntity<Object> handleCustomException(CustomException ex){
		 
		 String message = ex.getMessage();
		 
		 ErrorMessage errorMessage = new ErrorMessage(new Date(),message);
		 
	      return new ResponseEntity<>(errorMessage,new HttpHeaders(),HttpStatus.INTERNAL_SERVER_ERROR) ;
	    }
}
