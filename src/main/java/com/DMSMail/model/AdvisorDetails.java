package com.DMSMail.model;

public class AdvisorDetails {

	private String imanageId;
	
	private String userName;
	
	private String mobileNum;
	
	private String emailId;
	
	private String category;

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public String getImanageId() {
		return imanageId;
	}

	public void setImanageId(String imanageId) {
		this.imanageId = imanageId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	
	
}
