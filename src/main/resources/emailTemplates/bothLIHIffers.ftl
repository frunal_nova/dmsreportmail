<!doctype html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Special Offers | Life Insurance and Health Insurance</title>
<style type="text/css">
* {
	margin: 0;
	padding: 0;
}
@font-face {
    font-family: 'pf_encore_sans_proregular';
    src: url('pfencoresanspro-reg.eot');
    src: url('pfencoresanspro-reg.eot?#iefix') format('embedded-opentype'),
         url('pfencoresanspro-reg.woff2') format('woff2'),
         url('pfencoresanspro-reg.woff') format('woff'),
         url('pfencoresanspro-reg.svg#pf_encore_sans_proregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
p {
  text-rendering: optimizeLegibility;
  font-feature-settings: "kern" 1;
  font-kerning: normal;
  margin-left: 6%;
}
body, html {
	font-family: 'pf_encore_sans_proregular', 'verdana', 'ariel';
	width: 100%;
	background-color: #ffffff;
	margin: 0 auto;
	padding: 0;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
}
table {
	border-collapse: collapse;
}
table.centerme {
	margin-left: auto;
	margin-right: auto;
}
img {
	border: 0;
}
.resImage {
	border: 0;
	width: 100%;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
<center>
  <table border="0" cellspacing="0" cellpadding="0" style="max-width:1000px;width:100%;background:#ffffff;background-color:#ffffff;margin:0 auto;">
    <tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/LI_HI_01.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;border:0" alt="" title="">
        </center></td>
    </tr>
    <tr>
      <td align="left" valign="top" bgcolor="#ffffff" style="background:#ffffff!important;background-color:#ffffff!important;align:left;valign:top;">
          <p style="font-size:20px; font-weight:bold;align:left; valign:top;background:#ffffff;background-color:#ffffff">Dear ${staffName},</p>
        </td>
    </tr>
    <tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/LI_HI_03.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;border:0" alt="" title="">
        </center></td>
    </tr>
    <tr>
      <td align="center" valign="top" bgcolor="#ffffff" style="background:#ffffff!important;background-color:#ffffff!important;align:left;valign:top;">
      	  <span style="font-size:50px; font-weight:bold; color:#C91429;text-align:center;valign:top;background:#ffffff;background-color:#ffffff">${count}</span>
        </td>
    </tr>
    <tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/LI_HI_05.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;border:0" alt="" title="">
        </center></td>
    </tr>
    <tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/LI_HI_06.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;border:0" alt="" title="">
        </center></td>
    </tr>
    <tr>
      <td align="center" valign="top"><center>
          <a href="https://www.abcselect.adityabirlacapital.com/login?utm_campaign=offer_mailer&utm_source=email&utm_medium=mailer" target="_blank" class="apply"><img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/LI_HI_07.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;" alt="" title=""></a>
        </center></td>
    </tr>
    <tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/LI_HI_08.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;border:0" alt="" title="">
        </center></td>
    </tr>
    </table>
</center>
</body>
</html>