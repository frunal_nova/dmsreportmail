<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Emailer</title>
    <style>

        table {border-collapse: collapse !important;}
    </style>
</head>
<body>
    
<table align="center"   width="1000px" style="margin:0 auto;border:1px solid #ccc" border="0" Cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background: #fff;">
                
                <tr>
                    <td style="width: 40px;height:40px;">&nbsp;
                        
                    </td>
                    <td style="color:#902065; font-family: Arial, Helvetica, sans-serif;font-size: 40px;background: #fff;line-height: 80px;font-weight: 600;">
                      
                    </td>
                </tr>
    
            </table>
        </td>
    </tr>


    <tr>
    <td>
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background: #902065;">
            
            <tr>
                <td style="width: 40px;height:40px;">&nbsp;
                    
                </td>
                <td style="color:#fff; font-family: Arial, Helvetica, sans-serif;font-size: 20px;text-transform: uppercase;background: #902065;">
                    Select Honoured
                </td>
            </tr>

        </table>
    </td>
</tr>
<tr>
    <td style="height: 20px;">&nbsp;</td>
</tr>

<tr>
    <td>
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            
            <tr>
                <td style="width: 40px;height:40px;">
                   
                </td>
                <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;">
                   Dear <span style="font-weight: bold;"> ${varName} </span>

                </td>
            </tr>

        </table>
    </td>
</tr>

<tr>
    <td style="height: 20px;"></td>
</tr>

<tr>
    <td>
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            
            <tr>
                <td style="width: 40px;height:40px;">
                  
                </td>
                <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;">
                  Your SELECT dashboard as on : <span style="font-weight: bold;"> ${varDate} </span>
                </td>
            </tr>

        </table>
    </td>
</tr>
<tr>
    <td style="height: 5px;"></td>
</tr>

<tr>
    <td>
        <table role="presentation" cellspacing="0" cellpadding="0" style="width: 90%;margin-left: 32pt;margin-right: 40pt;border: 1px solid #CCC;">
            
            <tr>

                <td>
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background: #902065;">
                        
                        <tr>
                            <td style="width: 20px;height:40px;">
                              
                            </td>
                            <td style="color:#fff; font-family: Arial, Helvetica, sans-serif;font-size: 20px;height: 40px;text-transform: uppercase;">
                                Qualification Status
                             </td>
                        </tr>
            
                    </table>
                </td>


               
              
            </tr>

            <tr>
                <td>
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        
                        <tr>
                            <td style="width: 440px;height:40px;margin: 0 auto;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="400" align="center">
            
                                    <tr>
                                        <td style="width: 70%;height:40px;font-family: Arial, Helvetica, sans-serif;font-size: 20px;">
                                            Total Select Point 
                                        </td>
                                        <td style="color:#902065; font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 30%;font-weight: bold;">
                                         ${varPoints}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 70%;height:40px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;font-weight: 600;">
                                            Primary Points 
                                        </td>
                                        <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 30%;font-weight: bold;">
                                            ${varPriPoints}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 70%;height:40px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;font-weight: 600;">
                                            Total Secondary Points 
                                        </td>
                                        <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 30%;">
                                            ${varTotSecPts}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 80%;">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            
                                                <tr>
                                                    <td style="height:20px; width: 185px;">
                                                       Secondary Points :
                                                    </td>
                                                    <td style="height:20px;">
                                                        ${varSecPts}
                                                     </td>
                                                   
                                                </tr>
                                                <tr>
                                                    <td style="height:20px; width: 185px;">
                                                       Activation Points :
                                                    </td>
                                                    <td style="height:20px;">
                                                       ${varActPts}
                                                     </td>
                                                   
                                                </tr>
                                    
                                    
                                            </table>
                                        </td>
                                        <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 10%;text-transform: uppercase;">&nbsp;
                                         
                                        </td>
                                    </tr>
                        
                                </table>
                            </td>
                            <td style="width: 1px;background: #ccc;">
                               <hr>
                            </td>
                            <td style="width: 440px;height:40px;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="420" align="center">
            
                                    <tr>
                                        <td style="width: 200px;height:40px;font-family: Arial, Helvetica, sans-serif;font-size: 20px;font-weight: bold;">
                                            Current Level :   ${varCurLvl}
                                        </td>
                                     
                                    </tr>

                                    <tr>
                                        <td style="width: 100%;height:40px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;font-weight: 600;">
                                            Total Points outstanding for <span style="color: #902065;">  ${NxtLvl} </span>:  ${lvlPts}
                                        </td>
                                       
                                    </tr>

                                    <tr>
                                        <td style="width: 100%;height:40px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;font-weight: 600;">
                                            Min Secondary Points outstanding for <span style="color: #902065;">  ${minNxtLvl} </span>:  ${NxtLvlpts}
                                        </td>
                                      
                                    </tr>

                                    <tr>
                                        <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 30%;text-transform: uppercase;">&nbsp;
                                         
                                        </td>
                                    </tr>
                        
                                </table>
                            </td>
                        </tr>
            
                    </table>
                </td>
            </tr>

        </table>
    </td>
</tr>

<tr>
    <td style="height: 30px;"></td>
</tr>

<tr>
    <td>
        <table role="presentation" cellspacing="0" cellpadding="0" style="width: 90%;margin-left: 32pt;margin-right: 40pt;border: 1px solid #CCC;">
            
            <tr>
               <td>
                   <table align="center" style="width: 100%; background: #902065;">
                       <tr>
                           <td style="width: 20px;"></td>
                           <td style="color:#fff; font-family: Arial, Helvetica, sans-serif;font-size: 20px;height: 40px;text-transform: uppercase;">Business Empanelment </td>
                           <td style="color:#fff; font-family: Arial, Helvetica, sans-serif;font-size: 20px;height: 40px;text-transform: uppercase;text-align: right;">8/10</td>
                           <td style="width: 20px;"></td>
                        </tr>
                   </table>
               </td>
              
            </tr>

            <tr>
                <td>
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        
                        <tr>
                            <td style="width: 250px;height:40px;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="80%" style="margin-left:40pt" align="center">
            
                                    <tr>
                                        <td style="height: 10px;"></td>
                                    </tr>

                                    <tr>
                                        <td style="color:#902065; line-height: 35px;font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 30%;font-weight: bold;">
                                            Primary Business 
                                        </td>
                                       
                                    </tr>
                                    <tr>
                                        <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 30%;text-transform: uppercase;font-weight: bold;">
                                           ${PRIBIZ}
                                        </td>
                                       
                                    </tr>

                                    <tr>
                                        <td style="height: 10px;"></td>
                                    </tr>

                        
                                </table>
                            </td>
                            <td style="width: 1px;height:40px; background: #ccc;">
                               <hr>
                            </td>
                            <td style="width: 640px;height:40px;" align="center" style="text-align: center;margin: 0 auto;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            
                                  


                                    <tr>
                                        <td style="text-align: center;">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="80%" align="center">
            
                                                <tr>
                                                    <td style="height: 10px;"></td>
                                                </tr>
            
                                                <tr>
                                                    <td style="color:#902065; line-height: 35px;font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 30%;font-weight: bold;">
                                                        Secondary Business 
                                                    </td>
                                                   
                                                </tr>
                                                <tr>
                                                    <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 30%;text-transform: uppercase;font-weight: bold;">
                                                       ${SECONDARYBIZLIST}
                                                    </td>
                                                   
                                                </tr>
            
                                                <tr>
                                                    <td style="height: 10px;"></td>
                                                </tr>
            
                                    
                                            </table>
                                        </td>
                                        <td style="color:#000; font-family: Arial, Helvetica, sans-serif;font-size: 20px;width: 5%;text-transform: uppercase;">&nbsp;
                                         
                                        </td>
                                    </tr>
                        
                                </table>
                            </td>
                        </tr>
            
                    </table>
                </td>
            </tr>

        </table>
    </td>
</tr>
<tr>
    <td style="height: 30px;"></td>
</tr>



<tr>
    <td>
        <table role="presentation" cellspacing="0" cellpadding="0" style="width: 100%;margin-left: 32pt;">

          
            
            <tr>
                <td style="line-height: 20px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;">kindly refer to the detailed dashboard in the <strong>Select Advisor Portal.<strong> </td>
            </tr>
            <tr>
                <td style="height: 5px;"></td>
            </tr>
            
            <tr>
                <td style="line-height: 20px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;">Link: <a href="https://www.abcselect.adityabirlacapital.com/" target="_blank" style="color: #902065;font-weight: bold;text-decoration: none;"> https://www.abcselect.adityabirlacapital.com/</td>
            </tr>
           
            <tr>
                <td style="height: 20px;"></td>
            </tr>
            <tr>
                <td style="line-height: 20px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #000;font-weight: bold;">Regards,</td>
            </tr>
            
            <tr>
                <td style="line-height: 20px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #000;font-weight: bold;">Team Select.</td>
            </tr>

            <tr>
                <td style="height: 30px;"></td>
            </tr>
            <tr>
                <td style="line-height: 20px;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;">For internal circulation only</td>
            </tr>
           
            
        </table>
    </td>
</tr>





<tr>
    <td style="height: 5px;"></td>
</tr>

<tr>
    <td style="text-align: center;">
        <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/pointImg10.jpg" alt="" style="width: 100%;">
    </td>
</tr>


</table>


</body>
</html>