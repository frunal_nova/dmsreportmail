
<!doctype html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SAP Launch - Honoured</title>
<style type="text/css">
* {
	margin: 0;
	padding: 0;
}
@font-face {
    font-family: 'pf_encore_sans_proregular';
    src: url('pfencoresanspro-reg.eot');
    src: url('pfencoresanspro-reg.eot?#iefix') format('embedded-opentype'),
         url('pfencoresanspro-reg.woff2') format('woff2'),
         url('pfencoresanspro-reg.woff') format('woff'),
         url('pfencoresanspro-reg.svg#pf_encore_sans_proregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
p {
  text-rendering: optimizeLegibility;
  font-feature-settings: "kern" 1;
  font-kerning: normal;
  margin-left: 6%;
}
body, html {
	font-family: 'pf_encore_sans_proregular', 'verdana', 'ariel';
	width: 100%;
	background-color: #ffffff;
	margin: 0 auto;
	padding: 0;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
}
table {
	border-collapse: collapse;
}
table.centerme {
	margin-left: auto;
	margin-right: auto;
}
img {
	border: 0;
}
.resImage {
	border: 0;
	width: 100%;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
<center>
  <table border="0" cellspacing="0" cellpadding="0" style="max-width:1000px;width:100%;background:#fff;background-color:#fff;margin:0 auto;">
<tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/kams_01.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;" alt="" title="">
        </center></td>
    </tr>
    <tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/kams_02.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;" alt="" title="">
        </center></td>
    </tr>
    <tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/kams_03.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;" alt="" title="">
        </center></td>
    </tr>

<tr>
      <td align="left" valign="top">
      		<p>Your login details are: </p>
            <p>URL - <a href="https://app.powerbi.com/groups/272d906b-8ab5-46ca-aba1-153728e0765a/reports/3c32651d-f1b9-4785-b5e2-0702b40d96a9" target="_blank" class="apply"><strong>Click here for KAM dashboard</strong></a></p>
            <p>Password - Please use your ABC credentials to login</p>
        </td>
    </tr>
<tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/kams_05.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;" alt="" title="">
        </center></td>
    </tr>
    </table>
</center>
</body>
</html>