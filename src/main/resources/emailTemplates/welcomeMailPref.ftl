<!doctype html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SAP Launch - Honoured</title>
<style type="text/css">
* {
	margin: 0;
	padding: 0;
}
@font-face {
    font-family: 'pf_encore_sans_proregular';
    src: url('pfencoresanspro-reg.eot');
    src: url('pfencoresanspro-reg.eot?#iefix') format('embedded-opentype'),
         url('pfencoresanspro-reg.woff2') format('woff2'),
         url('pfencoresanspro-reg.woff') format('woff'),
         url('pfencoresanspro-reg.svg#pf_encore_sans_proregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
p {
  text-rendering: optimizeLegibility;
  font-feature-settings: "kern" 1;
  font-kerning: normal;
  margin-left: 6%;
  font-size: 20px;
  line-height: 1;
 
}
body, html {
	font-family: 'pf_encore_sans_proregular', 'verdana', 'ariel';
	width: 100%;
	background-color: #ffffff;
	margin: 0 auto;
	padding: 0;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
}
table {
	border-collapse: collapse;
}
table.centerme {
	margin-left: auto;
	margin-right: auto;
}
img {
	border: 0;
}
.resImage {
	border: 0;
	width: 100%;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
<center>
  <table border="0" cellspacing="0" cellpadding="0" style="max-width:1000px;width:100%;background:#fff;background-color:#fff;margin:0 auto;">
    <tr>
      <td align="left" valign="top">
          <p style="font-size:20px; font-weight:bold;">Dear ${staffName},</p>
<br>
<br>
        </td>
    </tr>
<tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/PASA_01.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;" alt="" title="">
        </center></td>
    </tr>
    
<br>
<tr>
      <td align="left" valign="top">
          <p font-weight:bold;">Your login details are:</p>

        </td>
    </tr>

        </td>
    </tr>

<tr>
      <td align="left" valign="top">
          
<a href="https://www.abcselect.adityabirlacapital.com/login" target="_blank" class="apply"><p style="font-weight:bold">URL - www.abcselect.adityabirlacapital.com</p></a>

<p style="font-weight:bold">User ID - Your Registered Email ID or mobile number</p>


<a href='${url}' target="_blank" class="apply"><p style="font-weight:bold">Password - click on this link to set your password</p></a>

        </td>
    </tr>

    <tr>
      <td align="center" valign="top"><center>
          <img src="https://www.abcselectuat.adityabirlacapital.com/mailerimages/PASA_02.jpg" class="resImage" border="0" style="padding:0; margin:0; display:block;" alt="" title="">
        </center></td>
    </tr>
    </table>
</center>
</body>
</html>